const puppeteer = require('puppeteer');

(async () => {
  const start = new Date().getTime()

  const url = 'http://localhost:8080/#/purchase/index'

  const browser = await puppeteer.launch({
    headless: true,
  });

  const login = async () => {
    const page = await browser.newPage();
    await page.goto(url, {waitUntil: 'networkidle2'});
    await page.type('[aria-label="用户名"]', '15272757750')
    await page.type('[aria-label="密码"]', '123456')
    await page.click('[type="submit"]')
    await page.waitForNavigation()
  }

  const testMobile = async () => {
    const devices = [
      'iPhone 7',
      'iPad'
    ]
    return Promise.all(
      devices.map(async name => {
        const page = await browser.newPage();
        const d = puppeteer.devices[name];
        await page.emulate(d);
        await page.goto(url, {waitUntil: 'networkidle2'});
        await page.screenshot({path: `screenshot/mobile/${name}.png`});
      })
    )
  }
  
  const testPC = async () => {
    const sizes = [
      { width: 1920, height: 1024 },
      { width: 1440, height: 1024 },
      { width: 1024, height: 1024 },
    ]
    return Promise.all(
      sizes.map(async size => {
        const page = await browser.newPage();
        await page.setViewport(size)
        await page.goto(url, {waitUntil: 'networkidle2'});
        await page.screenshot({path: `screenshot/pc/${size.width}x${size.height}.png`});
      })
    )
  }

  await login()

  await Promise.all([
    testMobile(),
    testPC(),
  ])

  await browser.close();

  const end = new Date().getTime()
  console.log(end - start)
})()
